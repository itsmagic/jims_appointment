﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Exchange.WebServices.Data;
using System.IO;
using System.Xml;


namespace Jims_getappointments
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ExchangeService service;
        private EmailAddressCollection myRoomLists;
        private String sSyncState;
        private Int32 sync_count;

        public MainWindow()
        { 
            InitializeComponent();
            //Set Sync State
            Sync_State_Box.Text = sSyncState;

            //Disable buttons before connection:
            Get_Rooms_Button.IsEnabled = false;
            AppointmentsButton.IsEnabled = false;
            Get_Room_List_Button.IsEnabled = false;
            Room_List_CMB.IsEnabled = false;
            From_Date.IsEnabled = false;
            To_Date.IsEnabled = false;
            Num_Appointments.IsEnabled = false;
            RoomAddress_Box.IsEnabled = false;
            Sync_Button.IsEnabled = false;
        }

        private void AppointmentsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                // Initialize the calendar folder object with only the folder ID. 
                //CalendarFolder calendar = CalendarFolder.Bind(service, WellKnownFolderName.Calendar, new PropertySet());
                FolderId folderid = new FolderId(WellKnownFolderName.Calendar, new Mailbox(RoomAddress_Box.Text));
                CalendarFolder calendar = CalendarFolder.Bind(service, folderid);

                // Set the start and end time and number of appointments to retrieve.
                CalendarView cView = new CalendarView(From_Date.SelectedDate.Value, To_Date.SelectedDate.Value, Convert.ToInt32(Num_Appointments.Text));

                // Limit the properties returned to the appointment's subject, start time, and end time.
                cView.PropertySet = new PropertySet(AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End);

                // Retrieve a collection of appointments by using the calendar view.
                FindItemsResults<Appointment> appointments = calendar.FindAppointments(cView);

                Console_Log.AppendText("\nThe first " + Convert.ToInt32(Num_Appointments.Text) + " appointments on your calendar from " + From_Date.SelectedDate.Value.ToShortDateString() +
                                  " to " + To_Date.SelectedDate.Value.ToShortDateString() + " are: \n");

                Console_Log.AppendText(Environment.NewLine);

                //StringBuilder for csv file
                var csv = new StringBuilder();



                foreach (Appointment a in appointments)
                {
                    //for log
                    Console_Log.AppendText("Subject: " + a.Subject.ToString() + " ");
                    Console_Log.AppendText(Environment.NewLine);
                    Console_Log.AppendText("Start: " + a.Start.ToString() + " ");
                    Console_Log.AppendText(Environment.NewLine);
                    Console_Log.AppendText("End: " + a.End.ToString());
                    Console_Log.AppendText(Environment.NewLine);
                    Console_Log.AppendText(Environment.NewLine);

                    //for file
                    var newLine = string.Format(
                        "{0},{1},{2},{3}{4}",
                        a.Subject.ToString(),
                        a.Id.ToString(),
                        a.Start.ToString(),
                        a.End.ToString(),
                        Environment.NewLine);
                    csv.Append(newLine);
                }
                string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                File.WriteAllText(filePath + "\\debug.csv", csv.ToString());
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString(), "An error has occured");
                Application.Current.Shutdown();
            }
            
        }

        private void RoomAddress_Box_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Clear_Log_Click(object sender, RoutedEventArgs e)
        {
            Console_Log.Clear();
        }

        private void Get_Rooms_Click(object sender, RoutedEventArgs e)
        {
            Rooms_List.Items.Clear();
            // Retrieve the room list that matches your criteria
            EmailAddress myAddress = new EmailAddress(Room_List_CMB.SelectedValue.ToString());

            // Expand the selected collection to get a list of rooms.
            System.Collections.ObjectModel.Collection<EmailAddress> myRoomAddresses = service.GetRooms(myAddress);

            // Display the individual rooms.
            foreach (EmailAddress address in myRoomAddresses)
            {
                //Console.WriteLine("Email Address: {0}", address.Address);
                Rooms_List.Items.Add(address.Address.ToString());
                Console_Log.AppendText("Email Adddress: " + address.Address.ToString());
                Console_Log.AppendText(Environment.NewLine);
            }
        }

        private void Connect_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                From_Date.SelectedDate = DateTime.Today;
                To_Date.SelectedDate = DateTime.Now.AddDays(1);
                //connect to EWS 
                service = new ExchangeService(ExchangeVersion.Exchange2010);

                //service.TraceEnabled = true;
                //service.TraceFlags = TraceFlags.All;
                service.Credentials = new WebCredentials(UserName_Box.Text, Password_Box.Password);
                service.Url = new Uri(User_Uri.Text);
                Console_Log.AppendText("EWS credentials / uri loaded");
                Console_Log.AppendText(Environment.NewLine);
                Connect_Button.IsEnabled = false;
                AppointmentsButton.IsEnabled = true;
                Get_Room_List_Button.IsEnabled = true;
                Room_List_CMB.IsEnabled = true;
                From_Date.IsEnabled = true;
                To_Date.IsEnabled = true;
                Num_Appointments.IsEnabled = true;
                RoomAddress_Box.IsEnabled = true;
                Sync_Button.IsEnabled = true;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString(), "An error has occured");
                Application.Current.Shutdown();
            }
        }

        private void Get_Room_List_Button_Click(object sender, RoutedEventArgs e)
        {
            // Return all the room lists in the organization.
            // This method call results in a GetRoomLists call to EWS.
            myRoomLists = service.GetRoomLists();

            // Display the room lists.
            foreach (EmailAddress address in myRoomLists)
            {
                Room_List_CMB.Items.Add(address.Address.ToString());

            }
            Room_List_CMB.SelectedIndex = 0;
            Get_Rooms_Button.IsEnabled = true;


        }
        private void Rooms_List_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (Rooms_List.SelectedItem != null)
            {
                RoomAddress_Box.Text = Rooms_List.SelectedItem.ToString();
                sSyncState = null;
            }
        }

        private void Sync_Button_Click(object sender, RoutedEventArgs e)
        {
            sync_count++;
            //StringBuilder for csv file
            var csv = new StringBuilder();
            try
            {
                // Initialize the flag that will indicate when there are no more changes.
                bool isEndOfChanges = false;

                // Call SyncFolderItems repeatedly until no more changes are available.
                // sSyncState represents the sync state value that was returned in the prior synchronization response.
                do
                {
                    // Get a list of changes (up to a maximum of 5) that have occurred on normal items in the Inbox folder since the prior synchronization.
                    //FolderId(WellKnownFolderName.Calendar, new Mailbox(RoomAddress_Box.Text)
                    FolderId folderid = new FolderId(WellKnownFolderName.Calendar, new Mailbox(RoomAddress_Box.Text));
                    ChangeCollection<ItemChange> icc = service.SyncFolderItems(folderid, PropertySet.FirstClassProperties, null, 5, SyncFolderItemsScope.NormalItems, Sync_State_Box.Text);

                    if (icc.Count == 0)
                    {
                        Console_Log.AppendText("There are no item changes to synchronize.");
                        Console_Log.AppendText(Environment.NewLine);
                    }
                    else
                    {
                        foreach (ItemChange ic in icc)
                        {
                            if (ic.ChangeType == ChangeType.Create)
                            {
                                //TODO: Create item on the client.
                            }
                            else if (ic.ChangeType == ChangeType.Update)
                            {
                                //TODO: Update item on the client.
                            }
                            else if (ic.ChangeType == ChangeType.Delete)
                            {
                                //TODO: Delete item on the client.
                            }
                            else if (ic.ChangeType == ChangeType.ReadFlagChange)
                            {
                                //TODO: Update the item's read flag on the client.
                            }
                            Console_Log.AppendText("ChangeType: " + ic.ChangeType.ToString());
                            Console_Log.AppendText(Environment.NewLine);
                            Console_Log.AppendText("ItemId: " + ic.ItemId.UniqueId);
                            Console_Log.AppendText(Environment.NewLine);
                            if (ic.Item != null)
                            {
                                Console_Log.AppendText("Subject: " + ic.Item.Subject);
                                Console_Log.AppendText(Environment.NewLine);
                            }
                            Console_Log.AppendText("===========");
                            Console_Log.AppendText(Environment.NewLine);
                             //for file
                            var newLine = string.Format(
                                "{0},{1},{2}{3}",
                                ic.ChangeType.ToString(),
                                ic.ItemId.UniqueId,
                                ic.Item.Subject,
                                Environment.NewLine);
                            csv.Append(newLine);
                            }
                    }

                    // Save the sync state for use in future SyncFolderHierarchy calls.
                    sSyncState = icc.SyncState;
                    Sync_State_Box.Text = sSyncState;
                    var newLine2 = string.Format(
                                "{0},{1}{2}",
                                "sSyncState",
                                sSyncState.ToString(),                                
                                Environment.NewLine);
                    csv.Append(newLine2);
                    string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    File.WriteAllText(filePath + "\\sync_number" + sync_count.ToString() + ".csv", csv.ToString());

                    if (!icc.MoreChangesAvailable)
                    {
                        isEndOfChanges = true;
                    }
                } while (!isEndOfChanges);

            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString(), "An error has occured");
                Application.Current.Shutdown();
            }
        }

        private void Appointment_Details_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Get_Appointment_Click(object sender, RoutedEventArgs e)
        {
            
            var appointment = Appointment.Bind(service, new ItemId(Appointment_Box.Text));
            Appointment_Details.AppendText("Start: " + appointment.Start.ToString());
            Appointment_Details.AppendText(Environment.NewLine);
            Appointment_Details.AppendText("End: " + appointment.End.ToString());
            Appointment_Details.AppendText(Environment.NewLine);
            Appointment_Details.AppendText("Subject: " + appointment.Subject.ToString());
            Appointment_Details.AppendText(Environment.NewLine);
            Appointment_Details.AppendText("Id: " + appointment.Id.ToString());
            Appointment_Details.AppendText(Environment.NewLine);
            Appointment_Details.AppendText("=================================");
            Appointment_Details.AppendText(Environment.NewLine);

        }

        private void Get_Appointment_Test_Button_Click(object sender, RoutedEventArgs e)
        {
            // Define search parameters and maximum number of items to return. 
            CalendarView calView = new CalendarView(From_Date.SelectedDate.Value, To_Date.SelectedDate.Value, 25);

            // Specify the IdOnly shape. 
            PropertySet props = new PropertySet(BasePropertySet.IdOnly);
            calView.PropertySet = props;

            // Call FindAppointments to search the Calendar folder. 
            FindItemsResults<Appointment> findResults = service.FindAppointments(WellKnownFolderName.Calendar, calView);

            if (findResults.TotalCount > 25)
            {
                string sMsg = "Total number of items that match search criteria exceeds specified maximum number of results to return.";
                sMsg += " To find all items that match the specified search criteria, either narrow the date range ";
                sMsg += "or increase the specified maximum number of items to return in CalendarView.";
                Exception error = new Exception(sMsg);
                throw error;
            }
            else
            {
                // Call LoadPropertiesForItems to load the Subject, RequiredAttendees, and OptionalAttendees properties. 
                service.LoadPropertiesForItems(from Item item in findResults select item,
                            new PropertySet(BasePropertySet.IdOnly,
                                    AppointmentSchema.Subject,
                                    AppointmentSchema.Start,
                                    AppointmentSchema.End,
                                    AppointmentSchema.Organizer,
                                    AppointmentSchema.IsAllDayEvent));
            }
            foreach (Appointment item in findResults.Items)
            {
                Appointment_Details.AppendText(item.Subject.ToString());
                Appointment_Details.AppendText(Environment.NewLine);
                Appointment_Details.AppendText(item.Start.ToString());
                Appointment_Details.AppendText(Environment.NewLine);
                Appointment_Details.AppendText(item.End.ToString());
                Appointment_Details.AppendText(Environment.NewLine);
                Appointment_Details.AppendText(item.Organizer.ToString());
                Appointment_Details.AppendText(Environment.NewLine);
                Appointment_Details.AppendText(item.IsAllDayEvent.ToString());
                Appointment_Details.AppendText(Environment.NewLine);
                Appointment_Details.AppendText("=======================================");
                Appointment_Details.AppendText(Environment.NewLine);
            }
            
            //Note: To use Linq as shown in the above example, you must include the following directive in your program: using System.Linq;















        }

    }
}
